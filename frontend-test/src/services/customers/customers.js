import axios from "axios";

const BASE_URL = "http://localhost:8080";

export const getUsers = () => {
  return axios
    .get(`${BASE_URL}/api/users`)
    .then((response) => JSON.stringify(response.data))
    .catch(() => Promise.reject("Error fetching users"));
};

export const addUser = (newUser) => {
  return axios
    .post(`${BASE_URL}/api/users`, newUser, {
      headers: { "Content-Type": "application/json" },
    })
    .then(({ status }) => status)
    .catch(() => Promise.reject("Error adding new user"));
};

export const deleteUser = (id) => {
  return axios
    .delete(`${BASE_URL}/api/users/${id}`)
    .then(({ status }) => status)
    .catch(() => Promise.reject("Error deleting user"));
};
