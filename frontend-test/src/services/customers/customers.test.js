import mockAxios from "axios";
import { getUsers, addUser, deleteUser } from "./customers";
import { data as customers } from "../../resources/testingSamples/customers";
import { data as createdCustomerResponse } from "../../resources/testingSamples/createdCustomerResponse";
import { data as deletedCustomerResponse } from "../../resources/testingSamples/deletedCustomerResponse";

const CREATED = 201;
const SUCCESS_DELETE = 200;

describe("Customers service tests", () => {
  beforeEach(() => {
    mockAxios.mockClear();
  });

  it("fetches users successfully", async () => {
    mockAxios.get.mockResolvedValue(customers);

    const users = await getUsers();

    expect(users).toEqual(customers.data);
  });

  it("fails when fetching users", async () => {
    const error = {
      response: {
        status: 500,
      },
    };
    mockAxios.get.mockRejectedValue();

    const users = getUsers(error);

    await expect(users).rejects.toBe("Error fetching users");
  });

  it("adds a new user successfully", async () => {
    const newUser = {
      name: "Vicky",
      description: "Software developer",
      photo: "http://some-pic.com",
    };

    mockAxios.post.mockResolvedValue(createdCustomerResponse);

    const status = await addUser(newUser);

    expect(status).toBe(CREATED);
  });

  it("fails when adding a new user", async () => {
    const error = {
      response: {
        status: 500,
      },
    };
    mockAxios.post.mockRejectedValue(error);

    const newUser = addUser(error);

    await expect(newUser).rejects.toBe("Error adding new user");
  });

  it("deletes user successfully", async () => {
    const userId = 7;

    mockAxios.delete.mockResolvedValue(deletedCustomerResponse);

    const response = await deleteUser(userId);

    expect(response).toBe(SUCCESS_DELETE);
  });

  it("fails when deleting a user", async () => {
    const error = {
      response: {
        status: 500,
      },
    };
    mockAxios.delete.mockRejectedValue(error);

    const response = deleteUser(error);

    await expect(response).rejects.toBe("Error deleting user");
  });
});
