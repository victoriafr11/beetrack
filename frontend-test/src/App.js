import { Provider } from "react-redux";
import "./App.css";
import store from "./store";
import CustomersContainer from "./containers/CustomersContainer";

function App() {
  return (
    <Provider store={store}>
      <CustomersContainer />
    </Provider>
  );
}

export default App;
