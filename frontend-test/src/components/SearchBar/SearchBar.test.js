import SearchBar from "./SearchBar";
import { render, screen } from "@testing-library/react";

describe("SearchBar tests", () => {
  it("renders a search bar with placeholder", () => {
    const component = <SearchBar />;
    render(component);

    const searchBar = screen.queryByPlaceholderText("Buscar contacto...");

    expect(searchBar).not.toBeNull();
  });

  it("renders a search bar with icon", () => {
    const component = <SearchBar />;
    render(component);

    const icon = screen.queryByAltText("buscar");

    expect(icon).not.toBeNull();
  });
});
