import React from "react";
import placeholders from "../../resources/texts/placeholders";
import { searchIcon } from "../../resources/icons";
import "./SearchBar.scss";

const SearchBar = ({ onChange }) => {
  return (
    <div className="searchBar">
      <img alt="buscar" src={searchIcon.default} />
      <input onChange={onChange} placeholder={placeholders.search} />
    </div>
  );
};

export default SearchBar;
