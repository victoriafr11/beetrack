import React, { useState } from "react";
import BeeButton from "../BeeButton/BeeButton";
import "./Form.scss";

const Form = ({ onSubmit }) => {
  const [profileUrl, setProfileUrl] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");

  const handlesProfileUrlValue = ({ target: { value } }) =>
    setProfileUrl(value);

  const handlesNameValue = ({ target: { value } }) => setName(value);

  const handlesDescriptionValue = ({ target: { value } }) =>
    setDescription(value);

  const handlesSubmit = () => {
    const newUser = { photo: profileUrl, name, description };
    validateFields(profileUrl, name, description) && onSubmit(newUser);
  };

  const validateFields = (profileUrl, name, description) =>
    profileUrl !== "" && name !== "" && description !== "";

  return (
    <div className="form-wrapper">
      <div className="form-header">
        <h2>Agregar nuevo contacto</h2>
      </div>
      <div className="form-content">
        <form>
          <label>
            URL Imagen de perfil
            <span>*</span>
          </label>
          <input
            type="text"
            name="profileUrl"
            onChange={handlesProfileUrlValue}
            required
          />
          <label>
            Nombre
            <span>*</span>
          </label>
          <input type="text" name="name" onChange={handlesNameValue} required />
          <label>
            Descripción
            <span>*</span>
          </label>
          <input
            type="text"
            name="description"
            onChange={handlesDescriptionValue}
            required
          />
          <div className="form-button">
            <BeeButton text="Guardar" onClick={handlesSubmit} />
          </div>
        </form>
      </div>
    </div>
  );
};

export default Form;
