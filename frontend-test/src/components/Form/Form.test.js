import Form from "./Form";
import { render, screen } from "@testing-library/react";

describe("Form tests", () => {
  it("renders form title", () => {
    const component = <Form />;
    render(component);

    const formTitle = screen.queryByText("Agregar nuevo contacto");

    expect(formTitle).not.toBeNull();
  });

  it("renders form fields", () => {
    const component = <Form />;
    render(component);

    const profileUrl = screen.queryByText("URL Imagen de perfil");
    const name = screen.queryByText("Nombre");
    const description = screen.queryByText("Descripción");

    expect(profileUrl).not.toBeNull();
    expect(name).not.toBeNull();
    expect(description).not.toBeNull();
  });

  it("renders button to add customer", () => {
    const component = <Form />;
    render(component);

    const button = screen.queryByText("Guardar");

    expect(button).not.toBeNull();
  });
});
