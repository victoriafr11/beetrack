import React, { useState } from "react";
import "./CustomersTable.scss";

const CustomersTable = ({ content, filterBy, deleteBy }) => {
  const [toggleLink, setToggleLink] = useState(false);

  const deleteUser = ({ target: { id } }) => deleteBy(id, content);

  const showsLink = (id) => setToggleLink(true);

  const hidesLink = () => setToggleLink(false);

  const renderRows = (customers) => {
    const filteredCustomers =
      filterBy !== undefined
        ? customers.filter(({ name }) =>
            name.toLowerCase().includes(filterBy.toLowerCase())
          )
        : customers;

    return filteredCustomers.map(({ id, name, description, photo }) => (
      <tr
        key={id}
        id={id}
        onMouseOver={() => showsLink(id)}
        onMouseLeave={() => hidesLink(id)}
      >
        <td>
          <img alt={`imagen ${name}`} src={photo} />
          <div>
            <p>{name}</p>
            {toggleLink ? (
              <a id={id} onClick={deleteUser}>
                Eliminar
              </a>
            ) : null}
          </div>
        </td>
        <td colSpan="2">{description}</td>
      </tr>
    ));
  };

  return (
    <table>
      <thead>
        <tr>
          <th>Nombre</th>
          <th colSpan="2">Descripción</th>
        </tr>
      </thead>
      <tbody>{renderRows(content)}</tbody>
    </table>
  );
};

export default CustomersTable;
