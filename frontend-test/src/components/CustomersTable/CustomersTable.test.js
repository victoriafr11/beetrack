import CustomersTable from "./CustomersTable";
import { data as customers } from "../../resources/testingSamples/customers";
import { fireEvent, render, screen } from "@testing-library/react";

describe("CustomersTable tests", () => {
  it("renders table headers", () => {
    const component = <CustomersTable content={customers} />;
    render(component);

    const nameHeader = screen.queryByText("Nombre");
    const descriptionHeader = screen.queryByText("Descripción");

    expect(nameHeader).not.toBeNull();
    expect(descriptionHeader).not.toBeNull();
  });

  it("renders customer row", () => {
    const component = <CustomersTable content={customers} />;
    render(component);

    const name = screen.queryByText("Francisco");
    const description = screen.queryByText(
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    );

    expect(name).not.toBeNull();
    expect(description).not.toBeNull();
  });

  it("renders customer picture", () => {
    const component = <CustomersTable content={customers} />;
    render(component);

    const profilePicture = screen.queryByAltText("imagen Francisco");

    expect(profilePicture).not.toBeNull();
  });

  it("renders row based on filter value", () => {
    const component = <CustomersTable content={customers} filterBy="Maria" />;
    render(component);

    const filteredCustomer = screen.queryByText("Maria");
    const anotherCustomer = screen.queryByText("Francisco");

    expect(filteredCustomer).not.toBeNull();
    expect(anotherCustomer).toBeNull();
  });

  it("renders delete link when customer row is hovered", () => {
    const component = <CustomersTable content={customers} />;
    render(component);

    const customer = screen.queryByText("Maria");

    fireEvent.mouseOver(customer);

    const deleteLinks = screen.queryAllByText("Eliminar");

    expect(deleteLinks).not.toBeNull();
  });
});
