import React from "react";
import "./BeeButton.scss";

const BeeButton = ({ text, children, onClick }) => {
  return (
    <>
      <button className="beebutton" onClick={onClick}>
        {children}
        {text}
      </button>
    </>
  );
};

export default BeeButton;
