import BeeButton from "./BeeButton";
import { render, screen, fireEvent } from "@testing-library/react";

describe("BeeButton tests", () => {
  it("renders a button texts only", () => {
    const component = <BeeButton text="Nuevo contacto" />;
    render(component);

    const buttonLabel = screen.queryByText("Nuevo contacto");

    expect(buttonLabel).not.toBeNull();
  });

  it("renders a button with left icon", () => {
    const component = (
      <BeeButton text="Nuevo contacto">
        <img alt="agregar" />
      </BeeButton>
    );
    render(component);

    const icon = screen.queryByAltText("agregar");

    expect(icon).not.toBeNull();
  });

  it("calls function when button is clicked", () => {
    const mockedFunction = jest.fn();

    const component = (
      <BeeButton text="Nuevo contacto" onClick={mockedFunction}>
        <img alt="agregar" />
      </BeeButton>
    );

    render(component);

    const button = screen.queryByText("Nuevo contacto");

    fireEvent.click(button);

    expect(mockedFunction).toHaveBeenCalledTimes(1);
  });
});
