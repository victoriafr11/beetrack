import React from "react";
import ReactDOM from "react-dom";
import "./Modal.scss";

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.modalRoot = document.getElementById("modal-root");
    this.modalElement = document.createElement("div");
    this.modalElement.className = "modal";
  }

  componentDidMount() {
    this.modalRoot.appendChild(this.modalElement);
  }

  componentWillUnmount() {
    this.modalRoot.removeChild(this.modalElement);
  }

  render() {
    return ReactDOM.createPortal(this.props.children, this.modalElement);
  }
}

export default Modal;
