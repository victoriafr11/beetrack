import { connect } from "react-redux";
import * as customersModule from "../modules/customers/customers";
import Form from "../components/Form/Form";

const mapStateToProps = (state) => ({
  isCreated: customersModule.isCreated(state),
});

const mapDispatchToProps = (dispatch) => ({
  onSubmit: (newCustomer) =>
    customersModule.createCustomerThunk(dispatch, newCustomer),
});

export default connect(mapStateToProps, mapDispatchToProps)(Form);
