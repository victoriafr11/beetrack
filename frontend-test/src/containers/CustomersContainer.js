import { connect } from "react-redux";
import * as customersModule from "../modules/customers/customers";
import Customers from "../screens/Customers/Customers";

const mapStateToProps = (state) => ({
  customers: customersModule.getCustomers(state),
});

const mapDispatchToProps = (dispatch) => ({
  onMount: () => customersModule.fetchCustomersThunk(dispatch),
  onDelete: (id, customers) =>
    customersModule.deleteCustomerThunk(dispatch, id, customers),
});

export default connect(mapStateToProps, mapDispatchToProps)(Customers);
