import React, { useState, useEffect } from "react";
import BeeButton from "../../components/BeeButton/BeeButton";
import buttons from "../../resources/texts/buttons";
import { addIcon } from "../../resources/icons";
import SearchBar from "../../components/SearchBar/SearchBar";
import CustomersTable from "../../components/CustomersTable/CustomersTable";
import FormContainer from "../../containers/FormContainer";
import Modal from "../../components/Modal/Modal";
import "./Customers.scss";

//import { data as customers } from "../../resources/testingSamples/customers";

const {
  add: { title },
} = buttons;

const Customers = ({ customers, onMount, onDelete }) => {
  const [customersData, setCustomersData] = useState([]);
  const [displayModal, setDisplayModal] = useState(false);
  const [filterValue, setFilterValue] = useState();

  useEffect(() => {
    onMount();
  }, [onMount]);

  useEffect(() => {
    if (customers && customers.length > 0) {
      setCustomersData(customers);
    }
  }, [customers]);

  const togglesModal = () => setDisplayModal((displayModal) => !displayModal);

  const rendersModal = () => {
    return (
      <Modal>
        <FormContainer />
      </Modal>
    );
  };

  const filterValues = ({ target: { value } }) => setFilterValue(value);

  return (
    <div className="container">
      {displayModal && rendersModal()}
      <h3>
        Test <span className="bold-title">Beetrack</span>
      </h3>
      <div className="options-wrapper">
        <SearchBar onChange={(event) => filterValues(event)} />
        <BeeButton text={title} onClick={togglesModal}>
          <img alt="agregar" src={addIcon.default} />
        </BeeButton>
      </div>
      <CustomersTable
        content={customersData}
        filterBy={filterValue}
        deleteBy={onDelete}
      />
    </div>
  );
};

export default Customers;
