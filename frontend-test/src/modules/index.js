import * as customersModule from "./customers/customers";

const reducers = {
  customers: customersModule.reducers,
};

export default {
  reducers,
};
