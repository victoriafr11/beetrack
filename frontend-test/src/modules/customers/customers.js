import {
  addUser,
  deleteUser,
  getUsers,
} from "../../services/customers/customers";

export const actions = {
  START_NEW_CUSTOMER_REQUEST: "START_NEW_CUSTOMER_REQUEST",
  FINISH_NEW_CUSTOMER_REQUEST: "FINISH_NEW_CUSTOMER_REQUEST",
  ERROR_NEW_CUSTOMER_REQUEST: "ERROR_NEW_CUSTOMER_REQUEST",
  START_CUSTOMERS_FETCH: "START_CUSTOMERS_FETCH",
  FINISH_CUSTOMERS_FETCH: "FINISH_CUSTOMERS_FETCH",
  ERROR_CUSTOMERS_FETCH: "ERROR_CUSTOMERS_FETCH",
  START_DELETE_CUSTOMER_REQUEST: "START_DELETE_CUSTOMER_REQUEST",
  FINISH_DELETE_CUSTOMER_REQUEST: "FINISH_DELETE_CUSTOMER_REQUEST",
  ERROR_DELETE_CUSTOMER_REQUEST: "ERROR_DELETE_CUSTOMER_REQUEST",
};

export const createCustomerThunk = (dispatch, customer) => {
  dispatch({ type: actions.START_NEW_CUSTOMER_REQUEST });
  const handlesSuccess = (response) => {
    dispatch({
      type: actions.FINISH_NEW_CUSTOMER_REQUEST,
      status: response,
    });
  };

  const handlesError = (response) => {
    dispatch({ type: actions.ERROR_NEW_CUSTOMER_REQUEST, status: response });
  };

  return addUser(customer).then(handlesSuccess).catch(handlesError);
};

export const fetchCustomersThunk = (dispatch) => {
  dispatch({ type: actions.START_CUSTOMERS_FETCH });
  const handlesSuccess = (response) => {
    dispatch({
      type: actions.FINISH_CUSTOMERS_FETCH,
      customers: JSON.parse(response),
    });
  };

  const handlesError = ({ response: { status } }) => {
    dispatch({ type: actions.ERROR_CUSTOMERS_FETCH, status });
  };

  return getUsers().then(handlesSuccess).catch(handlesError);
};

export const deleteCustomerThunk = (dispatch, id, customers) => {
  dispatch({ type: actions.START_DELETE_CUSTOMER_REQUEST });
  const handlesSuccess = (response) => {
    dispatch({
      type: actions.FINISH_DELETE_CUSTOMER_REQUEST,
      status: response === 200,
      customers: deleteUserBy(id, customers),
    });
  };

  const handlesError = (response) => {
    dispatch({ type: actions.ERROR_DELETE_CUSTOMER_REQUEST, status: response });
  };

  return deleteUser(id).then(handlesSuccess).catch(handlesError);
};

const deleteUserBy = (id, customers) =>
  customers.filter((element) => element.id !== parseInt(id));

const INITIAL_STATE = {
  isCreated: undefined,
  customers: [],
};

export const reducers = (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case actions.START_NEW_CUSTOMER_REQUEST:
      return {
        ...state,
        isCreated: false,
      };
    case actions.FINISH_NEW_CUSTOMER_REQUEST:
      return {
        ...state,
        isCreated: true,
      };
    case actions.FINISH_CUSTOMERS_FETCH:
      return {
        ...state,
        customers: action.customers,
      };
    case actions.FINISH_DELETE_CUSTOMER_REQUEST:
      return {
        customers: action.customers,
        isDeleted: action.status,
      };
    default:
      return {
        ...state,
      };
  }
};

export const isCreated = (state) => state.customers.isCreated;
export const getCustomers = (state) => state.customers.customers;
