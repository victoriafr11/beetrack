import * as module from "./customers";
import {
  addUser,
  deleteUser,
  getUsers,
} from "../../services/customers/customers";
import customers from "../../resources/testingSamples/customers";

jest.mock("../../services/customers/customers");

describe("Module: Customers tests", () => {
  describe("Thunks tests", () => {
    beforeEach(() => {
      addUser.mockClear();
      getUsers.mockClear();
    });

    it("dispatch start and finish actions when createCustomerThunk is called", async () => {
      const createdCustomerResponse = 201;
      const mockedDispatch = jest.fn();

      addUser.mockResolvedValue(createdCustomerResponse);

      await module.createCustomerThunk(mockedDispatch);

      expect(mockedDispatch.mock.calls[0][0].type).toBe(
        "START_NEW_CUSTOMER_REQUEST"
      );
      expect(mockedDispatch.mock.calls[1][0].type).toBe(
        "FINISH_NEW_CUSTOMER_REQUEST"
      );
      expect(mockedDispatch.mock.calls[1][0].status).toBe(201);
    });
    it("dispatch start and error actions when createCustomerThunk call fails", async () => {
      const error = 500;
      const mockedDispatch = jest.fn();

      addUser.mockRejectedValue(error);

      await module.createCustomerThunk(mockedDispatch);

      expect(mockedDispatch.mock.calls[0][0].type).toBe(
        "START_NEW_CUSTOMER_REQUEST"
      );
      expect(mockedDispatch.mock.calls[1][0].type).toBe(
        "ERROR_NEW_CUSTOMER_REQUEST"
      );
      expect(mockedDispatch.mock.calls[1][0].status).toBe(500);
    });

    it("dispatch start and finish actions when fetchCustomersThunk is called", async () => {
      const serverResponse = '[{ "id": 1, "name": "Vicky"}]';
      const mockedDispatch = jest.fn();

      getUsers.mockResolvedValue(serverResponse);

      await module.fetchCustomersThunk(mockedDispatch);

      expect(mockedDispatch.mock.calls[0][0].type).toBe(
        "START_CUSTOMERS_FETCH"
      );
      expect(mockedDispatch.mock.calls[1][0].type).toBe(
        "FINISH_CUSTOMERS_FETCH"
      );
      expect(mockedDispatch.mock.calls[1][0].customers).toEqual(
        JSON.parse(serverResponse)
      );
    });
    it("dispatch start and error actions when fetchCustomersThunk call fails", async () => {
      const error = {
        response: {
          status: 500,
        },
      };
      const mockedDispatch = jest.fn();

      getUsers.mockRejectedValue(error);

      await module.fetchCustomersThunk(mockedDispatch);

      expect(mockedDispatch.mock.calls[0][0].type).toBe(
        "START_CUSTOMERS_FETCH"
      );
      expect(mockedDispatch.mock.calls[1][0].type).toBe(
        "ERROR_CUSTOMERS_FETCH"
      );
      expect(mockedDispatch.mock.calls[1][0].status).toBe(500);
    });

    it("dispatch start and finish actions when deleteCustomerThunk is called", async () => {
      const customersResponse = [{ id: 1 }, { id: 3 }];
      const serverResponse = 200;
      const mockedDispatch = jest.fn();

      deleteUser.mockResolvedValue(serverResponse);

      await module.deleteCustomerThunk(mockedDispatch, 1, customersResponse);

      expect(mockedDispatch.mock.calls[0][0].type).toBe(
        "START_DELETE_CUSTOMER_REQUEST"
      );
      expect(mockedDispatch.mock.calls[1][0].type).toBe(
        "FINISH_DELETE_CUSTOMER_REQUEST"
      );
      expect(mockedDispatch.mock.calls[1][0].status).toBe(true);
      expect(mockedDispatch.mock.calls[1][0].customers).toEqual([{ id: 3 }]);
    });
  });
  describe("Reducers tests", () => {
    it("changes isCreated state to false when action START_NEW_CUSTOMER_REQUEST is executed", () => {
      const initialState = { isCreated: undefined };
      const action = { type: "START_NEW_CUSTOMER_REQUEST" };

      const { isCreated } = module.reducers(initialState, action);

      expect(isCreated).toBe(false);
    });
    it("changes isCreated state to true when action FINISH_NEW_CUSTOMER_REQUEST is executed", () => {
      const initialState = { isCreated: undefined };
      const action = { type: "FINISH_NEW_CUSTOMER_REQUEST" };

      const { isCreated } = module.reducers(initialState, action);

      expect(isCreated).toBe(true);
    });
    it("changes customers state action FINISH_CUSTOMERS_FETCH is executed", () => {
      const initialState = { customers: undefined };
      const action = { type: "FINISH_CUSTOMERS_FETCH", customers };

      const { customers: customersResponse } = module.reducers(
        initialState,
        action
      );

      expect(customersResponse).toEqual(customers);
    });
    it("changes isDeleted state action FINISH_DELETE_CUSTOMER_REQUEST is executed", () => {
      const initialState = { isDeleted: undefined };
      const action = {
        type: "FINISH_DELETE_CUSTOMER_REQUEST",
        status: true,
        customers: [{ id: 1 }, { id: 3 }],
      };

      const { isDeleted, customers } = module.reducers(initialState, action);

      expect(isDeleted).toBe(true);
      expect(customers).toBe(action.customers);
    });
  });
  describe("Selectors tests", () => {
    it("returns isCreated from state", () => {
      const state = { customers: { isCreated: true } };

      const isCreated = module.isCreated(state);

      expect(isCreated).toBe(true);
    });
    it("returns customers from state", () => {
      const state = { customers: { customers } };

      const customersResponse = module.getCustomers(state);

      expect(customersResponse).toEqual(customers);
    });
  });
});
